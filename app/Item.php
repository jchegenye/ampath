<?php 

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Item extends Eloquent
{
	
    protected $collection = "items";

    protected $fillable = ['title','description','m_name','m_price'];

}

/*<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Item extends Model
{

    public $fillable = ['title','description','m_name','m_price'];

}*/