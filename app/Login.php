<?php 

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Login extends Eloquent
{
	
    protected $collection = "users";

    protected $fillable = ['name','email','password','remember_token'];

}