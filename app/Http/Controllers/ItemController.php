<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Item;
use App\Meal;


/**
 * @author Jackson Asumu Chegenye <chegenyejackson@gmail.com> <0711494289>
 * @version 0.0.1
 * @copyright J-Tech Company KE <www.j-tech.tech>
 *
 * @File Controls Ampath Ordering System
 */

class ItemController extends Controller
{
    
    /**
     * Display orders Only.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $input = $request->all();

        if($request->get('search')){
            $items = Item::where("order_title", "LIKE", "%{$request->get('search')}%")
                ->paginate(5);      
        }else{

		    $items = Item::paginate(5);

        }

        return response($items);
    }

    /**
     * Display a listing of the resource either active or history.
     *
     * @return Response
     */
    public function ordersStatus(){

        return Response::json(Meal::get());
        return Response::json(Item::get());

    }

    /**
     * Store an image of the newly created order in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $checkUser = Item::where('user_uid','=', Auth::user()->uid)->get();
        if($checkUser){
            $order = new Item;
                $random_id = Item::orderBy('oid', 'DESC')->take(1)->get();
                if ($random_id->isEmpty()) {
                    $order->oid = 1;
                } else {
                    foreach ($random_id as $count) {
                        $id = $count->oid;
                        $order->oid = $id + 1;
                    }
                }
                $order->order_status = 'ordered';
                $order->order_title = Input::get('order_title');
                $order->order_description = Input::get('order_description');
                $order->order_date = date('M d, Y');
                $order->user_name = Auth::user()->name;
                $order->user_uid = Auth::user()->uid;
                $order->save();

            return response($order);
        }
    }

    /**
     * Show the form for editing the order resource.
     *
     * @param  int  $oid
     * @return Response
     */
    public function edit($oid)
    {
        $mealOrdered = Item::where('oid','=',(int)$oid)->first();
        return response($mealOrdered);
    }

    /**
     * Edit the specified order in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$oid)
    {
        
        $checkUser = Item::where('user_uid','=', Auth::user()->uid)->get();
        if($checkUser){

            $order = Item::where('oid','=',(int)$oid)->first();

            $mealOrdered = Item::where('oid','=',(int)$oid)->first();
                $mealOrdered->order_title = Input::get('order_title');
                $mealOrdered->order_description = Input::get('order_description');
                $mealOrdered->order_date = date('M d, Y');
                $mealOrdered->order_status = 'ordered';
                $mealOrdered->meal_name = Input::get('meal_name');
                $mealOrdered->meal_price = Input::get('meal_price');
                $mealOrdered->meal_date = date('M d, Y H:i T');
                $mealOrdered->meal_added_by = Auth::user()->name;
                $mealOrdered->user_name = Auth::user()->name;
                $mealOrdered->user_uid = Auth::user()->uid;
            $mealOrdered->save();

            return response($mealOrdered);
        }
    }

    /**
     * Remove the order resource from view with a soft delete.
     *
     * @param  int  $oid
     * @return Response
     */
    public function destroy($oid)
    {
        return Item::where('oid','=',(int)$oid)->delete();
    }

    /**
     * Remove the Meal resource from view with a soft delete.
     *
     * @param  int  $oid
     * @return Response
     */
    public function removeMeal($oid)
    {
        return Meal::where('oid','=',(int)$oid)->delete();
    }

    /**
     * Adding meal to order. Only one item per user in order.
     *
     * @param  int  $id
     * @return Response
     */
    public function addMeal(Request $request,$oid)
    {


        $checkOrder = Meal::where('user_uid','=', Auth::user()->uid)
            ->where('oid', '=', (int)$oid)
            ->first();
        if(empty($checkOrder->check)){

            $order = Item::where('oid','=',(int)$oid)->first();
            
            $mealOrdered = new Meal;
            $mealOrdered->order_status = $order->order_status;
            $mealOrdered->order_title = $order->order_title;
            $mealOrdered->order_description = $order->order_description;
            $mealOrdered->order_date = $order->order_date;
            $mealOrdered->user_name = $order->user_name;
            $mealOrdered->user_uid = $order->user_uid;

            $mealOrdered->check = 'yes';
            $mealOrdered->meal_name = Input::get('meal_name');
            $mealOrdered->meal_price = Input::get('meal_price');
            $mealOrdered->meal_date = date('M d, Y H:i T');
            $mealOrdered->meal_added_by = Auth::user()->name;
            $mealOrdered->user_name = Auth::user()->name;
            $mealOrdered->user_uid = Auth::user()->uid;
            $mealOrdered->oid = $order->oid;
            $mealOrdered->save();

            return response($mealOrdered);

        }elseif($checkOrder->check == 'yes' && $checkOrder->user_uid == Auth::user()->uid){

            return array('status'=>'Only one item per user in order!');

        }
        
    }

    /**
     * Change order's status to delivered.
     *
     * @param  int  $id
     * @return Response
     */
    public function delivered($oid)
    {
        $mealOrdered = Item::where('oid','=',(int)$oid)->first();
        if($mealOrdered){

            $mealOrdered->order_status = 'delivered';
            $mealOrdered->save();

        }

        return response($mealOrdered);
    }

}
