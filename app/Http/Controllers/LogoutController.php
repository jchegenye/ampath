<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


/**
 * @author Jackson Asumu Chegenye <chegenyejackson@gmail.com> <0711494289>
 * @version 0.0.1 
 * @copyright J-Tech Company KE <www.j-tech.tech>
 *
 * @File Handles Logout
 */


class LogoutController extends Controller {

    public function doLogout()
    {
        Auth::logout();

        $cookie = Cookie::forget('user_cookie');

        return Redirect::to('/')->withCookie($cookie);
    }

}