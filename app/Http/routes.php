<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('app');
    });
    
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', array('uses' => 'LogoutController@doLogout', 'before' => 'auth'));
    
    Route::get('home', function(){
        return view('home');
    });
    
    Route::get('orders', function(){
        View::addExtension('html','php');
    });


    /*
    |--------------------------------------------------------------------------
    | Application Routes
    |--------------------------------------------------------------------------
    |
    | This route group applies the "web" middleware group to every route
    | it contains. The "web" middleware group is defined in your HTTP
    | kernel and includes session state, CSRF protection, and more.
    |
    */

    Route::group(['middleware' => ['auth']], function () {

        Route::get('meal/{oid}', [
            'uses' => 'ItemController@removeMeal'
        ]);

        Route::resource('orders', 'ItemController@ordersStatus');

        Route::resource('items', 'ItemController');

        Route::get('status/{oid}', [
            'uses' => 'ItemController@delivered'
        ]);

        Route::post('meals/{oid}', [
            'uses' => 'ItemController@addMeal'
        ]);
    
        // Templates
        Route::group(array('prefix'=>'/templates/'),function(){
            Route::get('{template}', array( function($template)
            {
                $template = str_replace(".html","",$template);
                View::addExtension('html','php');
                return View::make('templates.'.$template);
            }));
        });

    });

});