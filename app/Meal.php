<?php 

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Meal extends Eloquent
{
	
    protected $collection = "meals";

}