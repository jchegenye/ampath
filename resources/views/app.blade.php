<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Ampath Kenya | Meal Ordering</title>

	<meta name="author" content="Chegenye Asumu Jackson">

	<!-- Bootstrap + Customized  Css -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="{{ asset('css/ampath.css') }}">

	<!-- Bootstrap + Customized Scripts -->
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/style.js') }}"></script>
	
	<!-- Angular JS -->
	<script src="{{ asset('angular-1.5.5/angular.min.js') }}"></script>  
	<script src="{{ asset('angular-1.5.5/angular-route.min.js') }}"></script>
	
	<!-- Order Scripts -->
	<script src="{{ asset('/app/packages/dirPagination.js') }}"></script>
	<script src="{{ asset('/app/routes.js') }}"></script>
	<script src="{{ asset('/app/services/myServices.js') }}"></script>
	<script src="{{ asset('/app/helper/myHelper.js') }}"></script>

	<!-- Order Controller -->
	<script src="{{ asset('/app/controllers/ItemController.js') }}"></script>
</head>

	<body>

		<!-- Navigation bar -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#/">Ampath Ke</a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="#/orders">Home</a></li>
						@if(Auth::check())
	                    <li><a href="#/orders">Orders</a></li>
		                @endif
		                @if(Auth::check())
		                <li><a href="#/orders">List</a></li>
		                @endif
					</ul>
					<ul class="nav navbar-nav navbar-right">
					@if(Auth::check())
		            <li><a href="{{url('auth/logout')}}">Sign out</a></li>      
	            	@endif
	            	</ul>
				</div>
			</div>
		</nav>

		<!-- Single Page Application - Ampath Meal Odering System  -->
		<div class="container">
			<div class="" ng-app="main-App">
				<div ng-view></div>
			</div>
		</div>

	</body>
</html>
