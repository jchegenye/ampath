<div class="container">
	<div class="row">
		<div class="col-md-8">
			<h2> Welcome to Ampath</h2>
			<p>
				A Meal Ordering Coordination System for Ampath developers.
			</p>

			<ul class="disk">
				<li>
					Every day, at 12:30PM, we decide where to order our meal from and gather orders on Slack.
				</li>
				<li>
					It's ineffective and sometimes orders are lost, that's why we would like to have an application to coordinate the process for us.
				</li>
				** This project is an assignment given by AmpathPlus Kenya for interview purposes.
			</ul>
			<hr>
			To login, kindly use any of the below credentials:
				<ol class="unstyled">
					<li>
						<b>Password:</b> jack@ampathdeveloper.com<br />
						<b>Email:</b> secret
					</li>
					<li>
						<b>Password:</b> nelly@ampathdeveloper.com<br />
						<b>Email:</b> secret
					</li>
				</ol>

			<hr>
			Laravel 5 REST API for the backend of the application & the powerfull of AngularJS for the frontend + Twitter Bootsrap 
		</div>
		<div class="col-md-4">
			<h4>LOGIN</h4>

			<form id="signup-form" action="{{url('/auth/login')}}" method="POST" class="signup-form">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
	            <div class="form-group ">
                      <input placeholder="Email Address"
                        class="form-control"
                        autocomplete="false" name="email" type="email" id="email"
                        value="{{ old('email') }}">
                    <span class="">
                        @if(Session::has('email')))
							<div class="alert alert-danger">
							  {{ Session::get('email')}} 
							</div>
						@endif
                    </span>
                </div>

				<div class="form-group ">
                    <input placeholder="Password"
                         class="form-control @if ($errors->has('password')) has-error @endif"
                         name="password" type="password"
                         id="password">
                    <span class="error-block">
                        @if($errors->has('password')){{ $errors->first('password') }} @endif
                    </span>
                </div>

	            <button type="submit" class="btn btn-primary">
	            	Sign in
	        	</button>
	        </form>

		</div>
	</div>
</div>