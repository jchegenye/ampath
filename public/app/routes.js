/**
 * @author Jackson Asumu Chegenye <chegenyejackson@gmail.com> <0711494289>
 * @version 0.0.1
 * @copyright J-Tech Company KE <www.j-tech.tech>
 *
 * @File Handles Routes For Ampath Ordering System
 */

var app =  angular.module('main-App',['ngRoute','angularUtils.directives.dirPagination']);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                restrict: 'EA',
                replace: 'true',
                templateUrl: 'home',
                controller: 'AdminController'
            }).
            when('/orders', {
                templateUrl: 'templates/items.html',
                controller: 'ItemController'
            }).
            when('/orders/active/', {
                templateUrl: 'templates/items.html',
                controller: 'ItemController'
            }).
            when('/list', {
                templateUrl: 'templates/items.html',
                controller: 'ItemController',
            }).
            when('/items/:index', {
                templateUrl: 'templates/items.html',
                controller: 'ItemController',
            });
}]);