/**
 * @author Jackson Asumu Chegenye <chegenyejackson@gmail.com> <0711494289>
 * @version 0.0.1
 * @copyright J-Tech Company KE
 *
 * @File Controls Ampath Ordering System <www.j-tech.tech>
 */

app.controller('ItemController', function(dataFactory,$scope,$http){
 
  $scope.data = [];
  $scope.libraryTemp = {};
  $scope.totalItemsTemp = {};

  $scope.totalItems = 0;
  $scope.pageChanged = function(newPage) {
    getResultsPage(newPage);
  };

  getResultsPage(1);
  function getResultsPage(pageNumber) {
      if(! $.isEmptyObject($scope.libraryTemp)){
          dataFactory.httpRequest('/items?search='+$scope.searchText+'&page='+pageNumber).then(function(data) {
            $scope.data = data.data;
            $scope.totalItems = data.total;
          });
      }else{
        dataFactory.httpRequest('/items?page='+pageNumber).then(function(data) {
          $scope.data = data.data;
          $scope.totalItems = data.total;
        });
      }
  }

  $scope.searchDB = function(){
      if($scope.searchText.length >= 3){
          if($.isEmptyObject($scope.libraryTemp)){
              $scope.libraryTemp = $scope.data;
              $scope.totalItemsTemp = $scope.totalItems;
              $scope.data = {};
          }
          getResultsPage(1);
      }else{
          if(! $.isEmptyObject($scope.libraryTemp)){
              $scope.data = $scope.libraryTemp ;
              $scope.totalItems = $scope.totalItemsTemp;
              $scope.libraryTemp = {};
          }
      }
  }

  $scope.saveAdd = function(){
    dataFactory.httpRequest('items','POST',{},$scope.form).then(function(data) {
      $scope.data.push(data);
      $(".modal").modal("hide");
    });
  }

  $scope.edit = function(oid){
    dataFactory.httpRequest('items/'+oid+'/edit').then(function(data) {
    	console.log(data);
      	$scope.form = data;
    });
  }

  $scope.saveEdit = function(){
    dataFactory.httpRequest('items/'+$scope.form.oid,'PUT',{},$scope.form).then(function(data) {
      	$(".modal").modal("hide");
        $scope.data = apiModifyTable($scope.data,data.oid,data);
    });
  }

  $scope.remove = function(item,index){
    var result = confirm("You are about to Delete this order, Continue...?");
   	if (result) {
      dataFactory.httpRequest('items/'+item.oid,'DELETE').then(function(data) {
          $scope.flash = data.status;
          $scope.data.splice(index,1);
      });
    }
  }

  $scope.removeMeal = function(item,index){
    var result = confirm("You are about to Delete this meal, Continue...?");
    if (result) {
      dataFactory.httpRequest('meal/'+item.oid,'GET').then(function(data) {
          $scope.status.push(data);
          $scope.flash = data.status;
          $scope.status.splice(index,1);
      });
    }
  }


  $scope.statusDelivered = function(item,index){
    var result = confirm("Mark this order as delivered?");
    if (result){
      dataFactory.httpRequest('status/'+item.oid,'GET',{},$scope.form).then(function(data) {
        console.log(data);
        $scope.form = data;
        $scope.data = apiModifyTable($scope.data,data.oid,data);
      });
    }
  }

  $scope.addMeal = function(){

    dataFactory.httpRequest('meals/'+$scope.form.oid,'POST',{},$scope.form).then(function(data) {
        $scope.flash = data.status;
        $scope.status.push(data);
        console.log(data);
        $scope.form = data;
        $scope.data = apiModifyTable($scope.data,data.oid,data);
        $(".modal").modal("hide");
      });

  }

  $http.get('orders').then(function(data) {
    console.log(data);
    $scope.status = data.data;
  });

   
});