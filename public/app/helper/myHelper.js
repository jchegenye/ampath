/**
 * @author Jackson Asumu Chegenye <chegenyejackson@gmail.com> <0711494289>
 * @version 0.0.1
 * @copyright J-Tech Company KE <www.j-tech.tech>
 *
 */

function apiModifyTable(originalData,oid,response){
    angular.forEach(originalData, function (item,key) {
        if(item.oid == oid){
            originalData[key] = response;
        }
    });
    return originalData;
}