<?php

use App\Login;
use Illuminate\Database\Seeder;
/*use Illuminate\Database\Eloquent\Model;*/

use Jenssegers\Mongodb\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UserTableSeeder::class);

		Model::unguard();

        $users = array(
            ['uid' => '1', 'name' => 'Jackson', 'email' => 'jack@ampathdeveloper.com', 'password' => Hash::make('secret'), 'remember_token'=> str_random(10)],
            ['uid' => '2', 'name' => 'Nelly', 'email' => 'nelly@ampathdeveloper.com', 'password' => Hash::make('secret'), 'remember_token'=> str_random(10)],
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            Login::create($user);
        }

        Model::reguard();

    }
}
