 <?php
namespace App\database\seeds\UserTableSeeder;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Model as Eloquent;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds for users login.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();
    	
       	DB::table('users')->insert([
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}